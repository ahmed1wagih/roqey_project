<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::group(['prefix' => '/{lang}','middleware' => ['web','uriLang']], function () {
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });


    Route::prefix('/admin')->group(function () {
        Route::get('/login/index', 'Admin\AuthController@index');
        Route::post('/login/check', 'Admin\AuthController@check');

        Route::middleware(['isAdmin'])->group(function () {
            Route::get('/dashboard', 'Admin\HomeController@index');

            Route::get('/admins/index', 'Admin\AdminController@index');
            Route::post('/admin/store', 'Admin\AdminController@store');
            Route::post('/admin/update', 'Admin\AdminController@update');
            Route::post('/admin/delete', 'Admin\AdminController@destroy');

            Route::get('/departments/index', 'Admin\DepartmentController@index');
            Route::post('/department/store', 'Admin\DepartmentController@store');
            Route::post('/department/update', 'Admin\DepartmentController@update');
            Route::post('/department/delete', 'Admin\DepartmentController@destroy');

            Route::get('/users/index', 'Admin\UserController@index');
            Route::post('/user/store', 'Admin\UserController@store');
            Route::post('/user/update', 'Admin\UserController@update');
            Route::post('/user/delete', 'Admin\UserController@destroy');

            Route::get('/logout', 'Admin\AuthController@logout');
        });
    });

    Route::prefix('/user')->group(function () {
        Route::get('/login/index', 'User\AuthController@index');
        Route::post('/login/check', 'User\AuthController@check');

        Route::middleware(['isUser'])->group(function () {
            Route::get('/dashboard', 'User\HomeController@index');

            Route::get('/department/index', 'User\DepartmentController@index');
        });

    });
});
