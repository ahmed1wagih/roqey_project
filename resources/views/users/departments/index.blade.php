@extends('users.layout.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/user/dashboard">{{trans('trans.dashboard')}}</a></li>
        <li class="active">{{trans('trans.departments')}}</li>

    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">

            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">

                     <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>{{trans('trans.ar_name')}}</th>
                                    <th>{{trans('trans.en_name')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$department->ar_name}}</td>
                                        <td>{{$department->en_name}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
