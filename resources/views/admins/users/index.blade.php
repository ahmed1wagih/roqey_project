@extends('admins.layout.app')
@section('content')
    <!-- START BREADCRUMB -->
    <ul class="breadcrumb">
        <li><a href="/admin/dashboard">{{trans('trans.dashboard')}}</a></li>
        <li class="active">{{trans('trans.users')}}</li>

    </ul>
    <!-- END BREADCRUMB -->

    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-12">
            <div class="alert alert-success" style="display: none;" id="outside_alert_success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <div class="alert alert-danger" style="display: none;" id="outside_alert_danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            </div>
            <!-- START BASIC TABLE SAMPLE -->
                <div class="panel panel-default">
                    <div class="panel-heading">

                        <button class="btn btn-info @if(lang() == 'ar') pull-left @else pull-right @endif" data-toggle="modal" data-target="#modal_create">{{trans('trans.create_new_user')}}</button>
                    </div>
                     <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table" id="users_table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('trans.department')}}</th>
                                    <th>{{trans('trans.ar_name')}}</th>
                                    <th>{{trans('trans.en_name')}}</th>
                                    <th>{{trans('trans.email')}}</th>
                                    <th>{{trans('trans.phone')}}</th>
                                    <th>{{trans('trans.operations')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr id="{{$user->id}}">
                                        <td class="id_td">{{$user->id}}</td>
                                        <td class="department_td" id="{{$user->department->id}}_dep">{{$user->department->name}}</td>
                                        <td class="ar_name_td edit_input">{{$user->ar_name}}</td>
                                        <td class="en_name_td edit_input">{{$user->en_name}}</td>
                                        <td class="email_td edit_input">{{$user->email}}</td>
                                        <td class="phone_td edit_input">{{$user->phone}}</td>
                                        <td class="edit_td">
                                            <button class="btn btn-warning btn-condensed edit_btn" data-toggle="modal" data-target="#modal_update"><i class="fa fa-edit"></i></button>
                                            <button class="btn btn-danger btn-condensed danger_btn"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                    <div class="modal" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="largeModalHead">{{trans('trans.create_new_user')}}</h4>
                                                </div>
                                                <span class="alert alert-danger" id="store_alert" role="alert" style="display: none;"></span>
                                                <form class="form-horizontal" method="post" id="add_user">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <select class="form-control select" name="department_id">
                                                                    <option selected disabled>{{trans('trans.select_department_please')}}</option>
                                                                    @forelse($departments as $department)
                                                                        <option value="{{$department->id}}">{{$department->name}}</option>
                                                                    @empty
                                                                        <option selected disabled>{{trans('trans.add_departments_first')}}</option>
                                                                    @endforelse

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="ar_name" id="ar_name_store" placeholder="{{trans('trans.ar_name')}}"/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="en_name" id="en_name_store" placeholder="{{trans('trans.en_name')}}"/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="email" class="form-control" name="email" id="email_store" placeholder="{{trans('trans.email')}}"/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="phone" id="phone_store" placeholder="{{trans('trans.phone')}}"/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="password" class="form-control" name="password" id="password_store" placeholder="{{trans('trans.password')}}"/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="password" class="form-control" name="password_confirmation" id="password_confirmation_store" placeholder="{{trans('trans.password_confirmation')}}"/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-success" id="submit_store" value="{{trans('trans.submit')}}">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('trans.close')}}</button>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="largeModalHead" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="largeModalHead">{{trans('trans.update_user')}}</h4>
                                            </div>
                                            <div class="alert alert-danger" id="update_alert" role="alert" style="display: none;"></div>
                                            <form class="form-horizontal" method="post" id="update_user">

                                                <div class="form-group">
                                                    <div class="col-md-12 col-xs-12">
                                                        <div class="input-group">
                                                            <select class="form-control"  name="department_id" id="department_id_update">
                                                                @forelse($departments as $department)
                                                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                                                @empty
                                                                    <option selected disabled>{{trans('trans.add_departments_first')}}</option>
                                                                @endforelse

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <br/>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <span id="inside_alert"></span>
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="ar_name" id="ar_name_update" value=""/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="en_name" id="en_name_update" value=""/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="email" id="email_update" value=""/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                    <br/>
                                                    <div class="form-group">
                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" name="phone" id="phone_update" value=""/>
                                                                <span class="input-group-addon"><span class="fa fa-info-circle"></span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" value="{{csrf_token()}}" name="_token">
                                                    <input type="hidden" value="" name="user_id" id="user_id">
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="submit" class="btn btn-success" id="submit_update" value="{{trans('trans.update')}}">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans('trans.close')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function()
        {
            $('#add_user').on('submit', function(event)
            {
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax
                (
                    {
                        url: '{{url(lang().'/admin/user/store')}}',
                        method : 'POST',
                        data : form_data,
                        dataType : 'json',
                        success : function (data)
                        {
                            if(data.status == 'success')
                            {
                                var row = '<tr id="'+data.data.id+'"><td class="id_td">'+data.data.id+'</td><td class="department_td edit_input">'+data.data.department_name+'</td><td class="ar_name_td edit_input">'+data.data.ar_name+'</td><td class="en_name_td edit_input">'+data.data.en_name+'</td><td class="email_td edit_input">'+data.data.email+'</td><td class="phone_td edit_input">'+data.data.phone+'</td><td class="edit_td"><button class="btn btn-warning btn-condensed edit_btn" data-toggle="modal" data-target="#modal_update"><i class="fa fa-edit"></i></button> <button class="btn btn-danger btn-condensed danger_btn" ><i class="fa fa-trash"></i></button></td></tr>';

                                $('#users_table tr:last').after(row);
                                $('#outside_alert_success').show();
                                $('#outside_alert_success').html(data.msg);
                                $('#modal_create').modal('hide');
                                $('#ar_name_store').val('');
                                $('#en_name_store').val('');
                                $('#email_store').val('');
                                $('#phone_store').val('');
                                $('#password_store').val('');
                                $('#password_confirmation_store').val('');
                            }
                            else
                            {
                                var errors = '';
                                for(var i = 0; i < data.msg.length; i++)
                                {
                                    errors += '<h3>' + data.msg[i] + '</h3>';
                                    $('#store_alert').html(errors);
                                    $('#store_alert').show();
                                }
                            }
                        }
                    }
                );
            });

            $('.edit_btn').on('click', function ()
            {
               var id = $(this).parent().siblings('.id_td').html()
                   department = $(this).parent().siblings('.department_td').attr('id')
                   department_id = $(this).parent().siblings('.department_td').attr('id').split('_dep')
                   ar_name = $(this).parent().siblings('.ar_name_td').html()
                   en_name = $(this).parent().siblings('.en_name_td').html()
                   email = $(this).parent().siblings('.email_td').html()
                   phone = $(this).parent().siblings('.phone_td').html();

               console.log(department,department_id[0],$('#department_id_update').val());
               $('#user_id').val(id);
               $('#department_id_update').val(department_id[0]);
               $('#ar_name_update').val(ar_name);
               $('#en_name_update').val(en_name);
               $('#email_update').val(email);
               $('#phone_update').val(phone);
            });

            $('#update_user').on('submit', function(event)
            {
                event.preventDefault();
                var form_data = $(this).serialize();
                $.ajax
                (
                    {
                        url: '{{url(lang().'/admin/user/update')}}',
                        method : 'POST',
                        data : form_data,
                        dataType : 'json',
                        success : function (data)
                        {
                            if(data.status == 'success')
                            {
                                $('#'+data.data.id+'').find($('.ar_name_td')).html(data.data.ar_name);
                                $('#'+data.data.id+'').find($('.department_td')).html(data.data.department_name);
                                $('#'+data.data.id+'').find($('.department_td')).attr('id',data.data.department_id);
                                $('#'+data.data.id+'').find($('.en_name_td')).html(data.data.en_name);
                                $('#'+data.data.id+'').find($('.email_td')).html(data.data.email);
                                $('#'+data.data.id+'').find($('.phone_td')).html(data.data.phone);

                                $('#outside_alert_success').show();
                                $('#outside_alert_success').html(data.msg);
                                $('#modal_update').modal('hide');
                                $('#admin_id').val('');
                                $('#ar_name_update').val('');
                                $('#en_name_update').val('');
                                $('#email_update').val('');
                                $('#phone_update').val('');
                            }
                            else
                            {
                                var errors = '';
                                for(var i = 0; i < data.msg.length; i++)
                                {
                                    errors += '<h3>' + data.msg[i] + '</h3>';
                                    $('#update_alert').html(errors);
                                    $('#update_alert').show();
                                }
                            }
                        }
                    }
                );
            });

            $('.edit_input').one('click', function()
            {
                var this_value = $(this).html()
                    this_name = $(this).attr('class').split('_td edit_input');
                    admin_id = $(this).closest('tr').attr('id');


                $(this).empty();
                $(this).html('<input type="text" class="form-control" name="'+this_name[0]+'" value="'+this_value+'"/><button class="btn btn-success update_btn">{{trans('trans.update')}}</button>');
            });

            $('.update_btn').on('click', function()
            {
                var id = $(this).parent().siblings('.id_td').html()
                    input = $(this).attr('name');
                    value = $($this).attr('value')
                    token = $('meta[name="csrf-token"]').attr('content');


                    console.log(id,input,value);
                $.ajax
                (
                    {
                        url: '{{url(lang().'/admin/user/update')}}',
                        method : 'POST',
                        data :
                            {
                                user_id : id,
                                input : value,
                                _token : token
                            },
                        dataType : 'json',
                        success : function (data)
                        {
                            if(data.status == 'success')
                            {
                                $('#'+data.data.id+'').find($('.ar_name_td')).html(data.data.ar_name);
                                $('#'+data.data.id+'').find($('.en_name_td')).html(data.data.en_name);
                                $('#'+data.data.id+'').find($('.email_td')).html(data.data.email);
                                $('#'+data.data.id+'').find($('.phone_td')).html(data.data.phone);

                                $('#outside_alert_success').show();
                                $('#outside_alert_success').html(data.msg);
                            }
                            else
                            {
                                var errors = '';
                                for(var i = 0; i < data.msg.length; i++)
                                {
                                    errors += '<h3>' + data.msg[i] + '</h3>';
                                    $('#update_alert_danger').html(errors);
                                    $('#update_alert_danger').show();
                                }
                            }
                        }
                    }
                );
            });

            $('.danger_btn').on('click', function ()
            {
                var id = $(this).parent().siblings('.id_td').html()
                token = $('meta[name="csrf-token"]').attr('content');

                $.ajax
                (
                    {
                        url: '{{url(lang().'/admin/user/delete')}}',
                        method : 'post',
                        data :
                            {
                                user_id : id,
                                _token: token
                            },
                        dataType : 'json',
                        success : function (data)
                        {
                            if(data.status == 'success')
                            {
                                $('#'+id).remove();
                                $('#outside_alert_success').html(data.msg);
                                $('#outside_alert_success').show();
                            }
                            else
                            {
                                var errors = '';
                                for(var i = 0; i < data.msg.length; i++)
                                {
                                    errors += '<h3>' + data.msg[i] + '</h3>';
                                    $('#outside_alert_danger').html(errors);
                                    $('#outside_alert_danger').show();
                                }
                            }
                        }
                    }
                );
            });
        });
    </script>
@endsection
