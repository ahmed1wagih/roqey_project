<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title>{{trans('trans.roqey')}} - {{trans('trans.login')}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="{{asset('/admin/img/favicon.ico')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    @if(lang() == 'ar')
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/theme-default_rtl.css')}}"/>
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/rtl.css')}}"/>
    @else
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/theme-default.css')}}"/>
    @endif
    <!-- EOF CSS INCLUDE -->
</head>
<body>

<div class="login-container">
    <div class="login-box animated fadeInDown">
        <div class="login-body">
            <div class="login-title"><strong>{{trans('trans.hello_please_login')}}</strong></div>
            <div class="login-title" style="color: red;"><strong>{{Session::get('error')}}</strong></div>
            <form action="/{{lang()}}/admin/login/check" class="form-horizontal" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="email" class="form-control" name="email" placeholder="Email"/>
                        @include('admins.layout.error', ['input' => 'email'])
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="password" class="form-control" name="password" placeholder="Password"/>
                        @include('admins.layout.error', ['input' => 'password'])
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <button class="btn btn-info btn-block">{{trans('trans.login')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

</body>
</html>

