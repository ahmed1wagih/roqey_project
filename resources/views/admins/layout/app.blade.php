<!DOCTYPE html>
<html lang="en">
<head>
    <!-- META SECTION -->
    <title>Project Admin - Dashboard</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset('admin/assets/images/users/avatar.jpg')}}" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    @if(lang() == 'ar')
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/theme-default_rtl.css')}}"/>
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/rtl.css')}}"/>
    @else
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('/admin/css/theme-default.css')}}"/>
    @endif
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery.min.js')}}"></script>
    <!-- START PLUGINS -->
    <script type="text/javascript" src="{{asset('admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>
    <!-- END PLUGINS -->
    <!-- EOF CSS INCLUDE -->


    <style>
        .image
        {
            height: 50px;
            width: 50px;
            border: 1px solid #29B2E1;
            border-radius: 100px;
            box-shadow: 2px 2px 2px darkcyan;
        }

        .input-group-addon {
            border-color: #33414e00 !important;
            background-color: #33414e00 !important;
            font-size: 13px;
            padding: 0px 0px 0px 3px;
            line-height: 26px;
            color: #FFF;
            text-align: center;
            min-width: 36px;
        }
    </style>

</head>
<body>
<!-- START PAGE CONTAINER -->
@if(lang() == 'ar')
    <div class="page-container page-mode-rtl page-content-rtl">
@else
    <div class="page-container page-mode-ltr page-content-ltr">
@endif
    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar page-sidebar-fixed scroll">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="/{{lang()}}/admin/dashboard">Project - Dashboard</a>
                <a href="#" class="x-navigation-control"></a>
            </li>

            <li class="xn-profile">
                <div class="profile">
                    <div class="profile-image">
                        <img src="/default_admin.jpg" alt="Admin"/>
                    </div>

                </div>
            </li>

            <li @if(Request::is(lang().'/admin/dashboard')) class="active" @endif>
                <a href="/{{lang()}}/admin/dashboard">
                    @if(lang() == 'ar')
                        <span class="xn-text">
                            {{trans('trans.dashboard')}}
                        </span>
                        <span class="fa fa-dashboard"></span>
                    @else
                        <span class="fa fa-dashboard"></span>
                        <span class="xn-text">
                            {{trans('trans.dashboard')}}
                        </span>
                    @endif
                </a>
            </li>

            <li @if(Request::is(lang().'admin/admins/*') xor Request::is(lang().'/admin/admin/*')) class="active" @endif>
                <a href="/{{lang()}}/admin/admins/index">
                    @if(lang() == 'ar')
                        <span class="xn-text">
                            {{trans('trans.admins')}}
                        </span>
                        <span class="fa fa-user-secret"></span>
                    @else
                        <span class="fa fa-user-secret"></span>
                        <span class="xn-text">
                            {{trans('trans.admins')}}
                        </span>
                    @endif

                </a>
            </li>

            <li @if(Request::is(lang().'admin/users/*') xor Request::is(lang().'/admin/user/*')) class="active" @endif>
                <a href="/{{lang()}}/admin/users/index">
                    @if(lang() == 'ar')
                        <span class="xn-text">
                            {{trans('trans.users')}}
                        </span>
                        <span class="fa fa-users"></span>
                    @else
                        <span class="fa fa-users"></span>
                        <span class="xn-text">
                            {{trans('trans.users')}}
                        </span>
                    @endif
                </a>
            </li>

            <li @if(Request::is(lang().'admin/departments/*') xor Request::is(lang().'/admin/department/*')) class="active" @endif>
                <a href="/{{lang()}}/admin/departments/index">
                    @if(lang() == 'ar')
                        <span class="xn-text">
                            {{trans('trans.departments')}}
                        </span>
                        <span class="fa fa-building"></span>
                    @else
                        <span class="fa fa-building"></span>
                        <span class="xn-text">
                            {{trans('trans.departments')}}
                        </span>
                    @endif
                </a>
            </li>


        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">
        <!-- START X-NAVIGATION VERTICAL -->
        @php
            $uri = Route::getCurrentRoute()->uri;
            $uri = explode('{lang}',$uri);

            Session::put('prev_url', $uri[1]);
        @endphp

        @if(lang() == 'ar')
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel pull-left last">
                <li class="xn-icon-button">
                    <a href="#" class="mb-control" data-box="#mb-signout" title="{{trans('trans.logout')}}"><span class="fa fa-power-off"></span></a>
                </li>
                <li>
                    <a href="#"><span class="fa fa-globe"></span></a>
                    <ul class="xn-drop-right xn-drop-white animated zoomIn">
                        @foreach(Config::get('languages') as $key => $value)
                            <li><a href="/lang/{{$key}}">{{$value}} <span class="fa fa-flag"></span></a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        @else
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel pull-right">
                <li class="xn-icon-button pull-right">
                    <a href="#" class="mb-control" data-box="#mb-signout" title="{{trans('trans.logout')}}"><span class="fa fa-power-off"></span></a>

                </li>
                <li class="xn-icon-button pull-right">
                    <a href="#"><span class="fa fa-globe"></span></a>
                    <ul class="xn-drop-left xn-drop-white animated zoomIn">
                        @foreach(Config::get('languages') as $key => $value)
                            <li><a href="/lang/{{$key}}"><span class="fa fa-flag"></span> {{$value}}</a></li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        @endif


        <!-- END X-NAVIGATION VERTICAL -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span><strong>{{trans('trans.logout')}}</strong> ?</div>
                    <div class="mb-content">
                        <p>{{trans('trans.logout_sure')}}</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="/{{lang()}}/admin/logout" class="btn btn-default btn-lg">{{trans('trans.yes')}}</a>
                            <button class="btn btn-primary btn-lg mb-control-close">{{trans('trans.no')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

     @yield('content')





<!-- START PRELOADS -->
<audio id="audio-alert" src="{{asset('admin/audio/alert.mp3')}}" preload="auto"></audio>
<audio id="audio-fail" src="{{asset('admin/audio/fail.mp3')}}" preload="auto"></audio>
<!-- END PRELOADS -->


<!-- THIS PAGE PLUGINS -->
<script type='text/javascript' src="{{asset('admin/js/plugins/icheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type='text/javascript' src='{{asset('admin/js/plugins/icheck/icheck.min.js')}}'></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/js/plugins/owl/owl.carousel.min.js')}}"></script>
<!-- END PAGE PLUGINS -->

<!-- START TEMPLATE -->
<script type="text/javascript" src="{{asset('admin/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/actions.js')}}"></script>


<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-file-input.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/bootstrap/bootstrap-select.js')}}"></script>
<script type="text/javascript" src="{{asset('admin/js/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
<!-- END THIS PAGE PLUGINS -->
<!-- END SCRIPTS -->
</body>
</html>






