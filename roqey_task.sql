-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 22, 2019 at 09:28 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `roqey_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `ar_name`, `en_name`, `email`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'الأدمن الرئيسي', 'Main Admin', 'admin@admin.com', '0123456789', '$2y$10$vpL09pRgQNMLwrBWxeB3COy2qcRZ6iQiIxEnxRc15E2LWhHWnEJqG', 'GGVoup0GKOJad4UbbEsWKz3VUF2J4wPEQB2kNWYCMofWIygl1JswksCR8w9N', '2019-02-20 22:00:00', '2019-02-20 22:00:00'),
(2, 'aaaaa', 'aaaaa', 'aaaaaaaaaaaaaaaaaaaa@aaa.com', '5555555555', '$2y$10$xYP42pA4uBoIo1MsWkejBuritHlC.Dn4cn.GB7f6vGJHEOAK/NoXW', NULL, '2019-02-22 16:25:39', '2019-02-22 17:00:27');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `ar_name`, `en_name`, `created_at`, `updated_at`) VALUES
(7, 'قسم قسم', 'department department', '2019-02-22 19:58:46', '2019-02-22 20:29:21'),
(9, 'جديد جديد', 'new new', '2019-02-22 21:09:49', '2019-02-22 21:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `ltm_translations`
--

DROP TABLE IF EXISTS `ltm_translations`;
CREATE TABLE IF NOT EXISTS `ltm_translations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `group` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `key` text COLLATE utf8mb4_bin NOT NULL,
  `value` text COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `ltm_translations`
--

INSERT INTO `ltm_translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'auth', 'failed', 'These credentials do not match our records.', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(2, 1, 'en', 'auth', 'throttle', 'Too many login attempts. Please try again in :seconds seconds.', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(3, 1, 'en', 'pagination', 'previous', '&laquo; Previous', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(4, 1, 'en', 'pagination', 'next', 'Next &raquo;', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(5, 1, 'en', 'passwords', 'password', 'Passwords must be at least six characters and match the confirmation.', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(6, 1, 'en', 'passwords', 'reset', 'Your password has been reset!', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(7, 1, 'en', 'passwords', 'sent', 'We have e-mailed your password reset link!', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(8, 1, 'en', 'passwords', 'token', 'This password reset token is invalid.', '2019-02-21 18:50:51', '2019-02-21 18:50:51'),
(9, 1, 'en', 'passwords', 'user', 'We can\'t find a user with that e-mail address.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(10, 1, 'en', 'validation', 'accepted', 'The :attribute must be accepted.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(11, 1, 'en', 'validation', 'active_url', 'The :attribute is not a valid URL.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(12, 1, 'en', 'validation', 'after', 'The :attribute must be a date after :date.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(13, 1, 'en', 'validation', 'after_or_equal', 'The :attribute must be a date after or equal to :date.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(14, 1, 'en', 'validation', 'alpha', 'The :attribute may only contain letters.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(15, 1, 'en', 'validation', 'alpha_dash', 'The :attribute may only contain letters, numbers, dashes and underscores.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(16, 1, 'en', 'validation', 'alpha_num', 'The :attribute may only contain letters and numbers.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(17, 1, 'en', 'validation', 'array', 'The :attribute must be an array.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(18, 1, 'en', 'validation', 'before', 'The :attribute must be a date before :date.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(19, 1, 'en', 'validation', 'before_or_equal', 'The :attribute must be a date before or equal to :date.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(20, 1, 'en', 'validation', 'between.numeric', 'The :attribute must be between :min and :max.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(21, 1, 'en', 'validation', 'between.file', 'The :attribute must be between :min and :max kilobytes.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(22, 1, 'en', 'validation', 'between.string', 'The :attribute must be between :min and :max characters.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(23, 1, 'en', 'validation', 'between.array', 'The :attribute must have between :min and :max items.', '2019-02-21 18:50:52', '2019-02-21 18:50:52'),
(24, 1, 'en', 'validation', 'boolean', 'The :attribute field must be true or false.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(25, 1, 'en', 'validation', 'confirmed', 'The :attribute confirmation does not match.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(26, 1, 'en', 'validation', 'date', 'The :attribute is not a valid date.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(27, 1, 'en', 'validation', 'date_equals', 'The :attribute must be a date equal to :date.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(28, 1, 'en', 'validation', 'date_format', 'The :attribute does not match the format :format.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(29, 1, 'en', 'validation', 'different', 'The :attribute and :other must be different.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(30, 1, 'en', 'validation', 'digits', 'The :attribute must be :digits digits.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(31, 1, 'en', 'validation', 'digits_between', 'The :attribute must be between :min and :max digits.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(32, 1, 'en', 'validation', 'dimensions', 'The :attribute has invalid image dimensions.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(33, 1, 'en', 'validation', 'distinct', 'The :attribute field has a duplicate value.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(34, 1, 'en', 'validation', 'email', 'The :attribute must be a valid email address.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(35, 1, 'en', 'validation', 'exists', 'The selected :attribute is invalid.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(36, 1, 'en', 'validation', 'file', 'The :attribute must be a file.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(37, 1, 'en', 'validation', 'filled', 'The :attribute field must have a value.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(38, 1, 'en', 'validation', 'gt.numeric', 'The :attribute must be greater than :value.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(39, 1, 'en', 'validation', 'gt.file', 'The :attribute must be greater than :value kilobytes.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(40, 1, 'en', 'validation', 'gt.string', 'The :attribute must be greater than :value characters.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(41, 1, 'en', 'validation', 'gt.array', 'The :attribute must have more than :value items.', '2019-02-21 18:50:53', '2019-02-21 18:50:53'),
(42, 1, 'en', 'validation', 'gte.numeric', 'The :attribute must be greater than or equal :value.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(43, 1, 'en', 'validation', 'gte.file', 'The :attribute must be greater than or equal :value kilobytes.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(44, 1, 'en', 'validation', 'gte.string', 'The :attribute must be greater than or equal :value characters.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(45, 1, 'en', 'validation', 'gte.array', 'The :attribute must have :value items or more.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(46, 1, 'en', 'validation', 'image', 'The :attribute must be an image.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(47, 1, 'en', 'validation', 'in', 'The selected :attribute is invalid.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(48, 1, 'en', 'validation', 'in_array', 'The :attribute field does not exist in :other.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(49, 1, 'en', 'validation', 'integer', 'The :attribute must be an integer.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(50, 1, 'en', 'validation', 'ip', 'The :attribute must be a valid IP address.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(51, 1, 'en', 'validation', 'ipv4', 'The :attribute must be a valid IPv4 address.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(52, 1, 'en', 'validation', 'ipv6', 'The :attribute must be a valid IPv6 address.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(53, 1, 'en', 'validation', 'json', 'The :attribute must be a valid JSON string.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(54, 1, 'en', 'validation', 'lt.numeric', 'The :attribute must be less than :value.', '2019-02-21 18:50:54', '2019-02-21 18:50:54'),
(55, 1, 'en', 'validation', 'lt.file', 'The :attribute must be less than :value kilobytes.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(56, 1, 'en', 'validation', 'lt.string', 'The :attribute must be less than :value characters.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(57, 1, 'en', 'validation', 'lt.array', 'The :attribute must have less than :value items.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(58, 1, 'en', 'validation', 'lte.numeric', 'The :attribute must be less than or equal :value.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(59, 1, 'en', 'validation', 'lte.file', 'The :attribute must be less than or equal :value kilobytes.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(60, 1, 'en', 'validation', 'lte.string', 'The :attribute must be less than or equal :value characters.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(61, 1, 'en', 'validation', 'lte.array', 'The :attribute must not have more than :value items.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(62, 1, 'en', 'validation', 'max.numeric', 'The :attribute may not be greater than :max.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(63, 1, 'en', 'validation', 'max.file', 'The :attribute may not be greater than :max kilobytes.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(64, 1, 'en', 'validation', 'max.string', 'The :attribute may not be greater than :max characters.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(65, 1, 'en', 'validation', 'max.array', 'The :attribute may not have more than :max items.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(66, 1, 'en', 'validation', 'mimes', 'The :attribute must be a file of type: :values.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(67, 1, 'en', 'validation', 'mimetypes', 'The :attribute must be a file of type: :values.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(68, 1, 'en', 'validation', 'min.numeric', 'The :attribute must be at least :min.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(69, 1, 'en', 'validation', 'min.file', 'The :attribute must be at least :min kilobytes.', '2019-02-21 18:50:55', '2019-02-21 18:50:55'),
(70, 1, 'en', 'validation', 'min.string', 'The :attribute must be at least :min characters.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(71, 1, 'en', 'validation', 'min.array', 'The :attribute must have at least :min items.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(72, 1, 'en', 'validation', 'not_in', 'The selected :attribute is invalid.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(73, 1, 'en', 'validation', 'not_regex', 'The :attribute format is invalid.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(74, 1, 'en', 'validation', 'numeric', 'The :attribute must be a number.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(75, 1, 'en', 'validation', 'present', 'The :attribute field must be present.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(76, 1, 'en', 'validation', 'regex', 'The :attribute format is invalid.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(77, 1, 'en', 'validation', 'required', 'The :attribute field is required.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(78, 1, 'en', 'validation', 'required_if', 'The :attribute field is required when :other is :value.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(79, 1, 'en', 'validation', 'required_unless', 'The :attribute field is required unless :other is in :values.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(80, 1, 'en', 'validation', 'required_with', 'The :attribute field is required when :values is present.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(81, 1, 'en', 'validation', 'required_with_all', 'The :attribute field is required when :values are present.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(82, 1, 'en', 'validation', 'required_without', 'The :attribute field is required when :values is not present.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(83, 1, 'en', 'validation', 'required_without_all', 'The :attribute field is required when none of :values are present.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(84, 1, 'en', 'validation', 'same', 'The :attribute and :other must match.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(85, 1, 'en', 'validation', 'size.numeric', 'The :attribute must be :size.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(86, 1, 'en', 'validation', 'size.file', 'The :attribute must be :size kilobytes.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(87, 1, 'en', 'validation', 'size.string', 'The :attribute must be :size characters.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(88, 1, 'en', 'validation', 'size.array', 'The :attribute must contain :size items.', '2019-02-21 18:50:56', '2019-02-21 18:50:56'),
(89, 1, 'en', 'validation', 'starts_with', 'The :attribute must start with one of the following: :values', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(90, 1, 'en', 'validation', 'string', 'The :attribute must be a string.', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(91, 1, 'en', 'validation', 'timezone', 'The :attribute must be a valid zone.', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(92, 1, 'en', 'validation', 'unique', 'The :attribute has already been taken.', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(93, 1, 'en', 'validation', 'uploaded', 'The :attribute failed to upload.', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(94, 1, 'en', 'validation', 'url', 'The :attribute format is invalid.', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(95, 1, 'en', 'validation', 'uuid', 'The :attribute must be a valid UUID.', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(96, 1, 'en', 'validation', 'custom.attribute-name.rule-name', 'custom-message', '2019-02-21 18:50:57', '2019-02-21 18:50:57'),
(97, 0, 'ar', 'trans', 'ar_name', 'الإسم بالعربية', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(98, 0, 'ar', 'trans', 'en_name', 'الإسم بالإنجليزية', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(99, 0, 'ar', 'trans', 'email', 'البريد الإلكتروني', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(100, 0, 'ar', 'trans', 'phone', 'الهاتف', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(101, 0, 'ar', 'trans', 'password', 'كلمة المرور', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(102, 0, 'ar', 'trans', 'login', 'تسجيل الدخول', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(103, 0, 'ar', 'trans', 'hello', 'أهلاً', '2019-02-21 18:51:35', '2019-02-22 21:00:12'),
(104, 0, 'ar', 'trans', 'password_confirmation', 'تأكيد كلمة المرور', '2019-02-21 18:52:27', '2019-02-22 21:00:12'),
(105, 0, 'en', 'trans', 'ar_name', 'Arabic Name', '2019-02-21 18:52:36', '2019-02-22 21:00:12'),
(106, 0, 'en', 'trans', 'email', 'Email', '2019-02-21 18:52:39', '2019-02-22 21:00:12'),
(107, 0, 'en', 'trans', 'en_name', 'English Name', '2019-02-21 18:52:44', '2019-02-22 21:00:12'),
(108, 0, 'en', 'trans', 'hello', 'Hello', '2019-02-21 18:52:46', '2019-02-22 21:00:12'),
(109, 0, 'en', 'trans', 'login', 'Login', '2019-02-21 18:52:50', '2019-02-22 21:00:12'),
(110, 0, 'en', 'trans', 'password', 'Password', '2019-02-21 18:52:56', '2019-02-22 21:00:12'),
(111, 0, 'en', 'trans', 'password_confirmation', 'Password Confirmation', '2019-02-21 18:53:01', '2019-02-22 21:00:12'),
(112, 0, 'en', 'trans', 'phone', 'Phone', '2019-02-21 18:53:04', '2019-02-22 21:00:12'),
(113, 0, 'ar', 'trans', 'roqey', 'رقي', '2019-02-21 18:54:02', '2019-02-22 21:00:12'),
(114, 0, 'en', 'trans', 'roqey', 'Roqey', '2019-02-21 18:54:06', '2019-02-22 21:00:12'),
(115, 0, 'ar', 'trans', 'hello_please_login', 'أهلاً, يرجي تسجيل الدخول', '2019-02-21 18:57:16', '2019-02-22 21:00:12'),
(116, 0, 'en', 'trans', 'hello_please_login', 'Hello, please login in .', '2019-02-21 18:57:25', '2019-02-22 21:00:12'),
(117, 0, 'ar', 'trans', 'logout', 'تسجل الخروج', '2019-02-21 19:04:50', '2019-02-22 21:00:12'),
(118, 0, 'en', 'trans', 'logout', 'Log out', '2019-02-21 19:04:57', '2019-02-22 21:00:12'),
(119, 0, 'ar', 'trans', 'logout_sure', 'هل أنت متأكد من تسجيل الخروج ؟', '2019-02-21 19:06:29', '2019-02-22 21:00:12'),
(120, 0, 'en', 'trans', 'logout_sure', 'Are you sure you want to log out?', '2019-02-21 19:06:33', '2019-02-22 21:00:12'),
(121, 0, 'ar', 'trans', 'yes', 'نعم', '2019-02-21 19:07:53', '2019-02-22 21:00:12'),
(122, 0, 'ar', 'trans', 'no', 'لا', '2019-02-21 19:07:53', '2019-02-22 21:00:12'),
(123, 0, 'en', 'trans', 'no', 'No', '2019-02-21 19:07:59', '2019-02-22 21:00:12'),
(124, 0, 'en', 'trans', 'yes', 'Yes', '2019-02-21 19:08:01', '2019-02-22 21:00:12'),
(125, 0, 'ar', 'trans', 'dashboard', 'الرئيسية', '2019-02-21 19:13:07', '2019-02-22 21:00:12'),
(126, 0, 'ar', 'trans', 'users', 'المستخدمين', '2019-02-21 19:13:07', '2019-02-22 21:00:12'),
(127, 0, 'ar', 'trans', 'departments', 'الأقسام', '2019-02-21 19:13:07', '2019-02-22 21:00:12'),
(128, 0, 'en', 'trans', 'dashboard', 'Dashboard', '2019-02-21 19:13:12', '2019-02-22 21:00:12'),
(129, 0, 'en', 'trans', 'departments', 'Departments', '2019-02-21 19:13:17', '2019-02-22 21:00:12'),
(130, 0, 'en', 'trans', 'users', 'Users', '2019-02-21 19:13:31', '2019-02-22 21:00:12'),
(131, 0, 'ar', 'trans', 'admins', 'المدراء', '2019-02-21 19:13:51', '2019-02-22 21:00:12'),
(132, 0, 'en', 'trans', 'admins', 'Admins', '2019-02-21 19:13:55', '2019-02-22 21:00:12'),
(133, 0, 'ar', 'trans', 'submit', 'إضافة', '2019-02-21 20:47:21', '2019-02-22 21:00:12'),
(134, 0, 'ar', 'trans', 'close', 'إغلاق', '2019-02-21 20:47:22', '2019-02-22 21:00:12'),
(135, 0, 'en', 'trans', 'close', 'Close', '2019-02-21 20:47:26', '2019-02-22 21:00:12'),
(136, 0, 'en', 'trans', 'submit', 'Submit', '2019-02-21 20:47:30', '2019-02-22 21:00:12'),
(137, 0, 'ar', 'trans', 'create_new_admin', 'إضافة مدير جديد', '2019-02-21 20:56:02', '2019-02-22 21:00:12'),
(138, 0, 'en', 'trans', 'create_new_admin', 'Create New Admin', '2019-02-21 20:56:16', '2019-02-22 21:00:12'),
(139, 0, 'ar', 'trans', 'operations', 'العمليات', '2019-02-21 21:05:45', '2019-02-22 21:00:12'),
(140, 0, 'en', 'trans', 'operations', 'Operations', '2019-02-21 21:05:49', '2019-02-22 21:00:12'),
(141, 0, 'ar', 'trans', 'admin_added', 'تم إضافة المدير بنجاح', '2019-02-21 21:08:14', '2019-02-22 21:00:12'),
(142, 0, 'en', 'trans', 'admin_added', 'Admin Added Successfully', '2019-02-21 21:08:25', '2019-02-22 21:00:12'),
(143, 0, 'ar', 'trans', 'update_admin', 'تعديل مدير', '2019-02-21 21:16:32', '2019-02-22 21:00:12'),
(144, 0, 'en', 'trans', 'update_admin', 'Update Admin', '2019-02-21 21:16:39', '2019-02-22 21:00:12'),
(145, 0, 'ar', 'trans', 'update', 'تعديل', '2019-02-21 21:18:03', '2019-02-22 21:00:12'),
(146, 0, 'en', 'trans', 'update', 'Update', '2019-02-21 21:18:06', '2019-02-22 21:00:12'),
(147, 0, 'ar', 'trans', 'admin_updated', 'تم تعديل المدير بنجاح', '2019-02-21 22:23:43', '2019-02-22 21:00:12'),
(148, 0, 'en', 'trans', 'admin_updated', 'Admin Updated Successfully', '2019-02-21 22:23:50', '2019-02-22 21:00:12'),
(149, 0, 'ar', 'trans', 'admin_deleted', 'تم مسح المدير بنجاح', '2019-02-22 16:49:14', '2019-02-22 21:00:12'),
(150, 0, 'en', 'trans', 'admin_deleted', 'Admin Deleted Successfully', '2019-02-22 16:49:23', '2019-02-22 21:00:12'),
(151, 0, 'ar', 'trans', 'department_added', 'تم إنشاء القسم بنجاح', '2019-02-22 19:10:49', '2019-02-22 21:00:12'),
(152, 0, 'ar', 'trans', 'department_updated', 'تم حذف القسم بنجاح', '2019-02-22 19:10:49', '2019-02-22 21:00:12'),
(153, 0, 'ar', 'trans', 'department_deleted', 'تم تعديل القسم بنجاح', '2019-02-22 19:10:49', '2019-02-22 21:00:12'),
(154, 0, 'en', 'trans', 'department_added', 'Department Added Successfully', '2019-02-22 19:11:08', '2019-02-22 21:00:12'),
(155, 0, 'en', 'trans', 'department_deleted', 'Department Updated Successfully', '2019-02-22 19:11:13', '2019-02-22 21:00:12'),
(156, 0, 'en', 'trans', 'department_updated', 'Department Deleted Successfully', '2019-02-22 19:11:18', '2019-02-22 21:00:12'),
(157, 0, 'ar', 'trans', 'user_added', 'تم إضافة المستخدم بنجاح', '2019-02-22 19:11:58', '2019-02-22 21:00:12'),
(158, 0, 'ar', 'trans', 'user_updated', 'تم حذف المستخدم بنجاح', '2019-02-22 19:11:58', '2019-02-22 21:00:12'),
(159, 0, 'ar', 'trans', 'user_deleted', 'تم تعديل المستخدم بنجاح', '2019-02-22 19:11:58', '2019-02-22 21:00:12'),
(160, 0, 'en', 'trans', 'user_added', 'User Added Successfully', '2019-02-22 19:12:10', '2019-02-22 21:00:12'),
(161, 0, 'en', 'trans', 'user_deleted', 'User Updated Successfully', '2019-02-22 19:12:14', '2019-02-22 21:00:12'),
(162, 0, 'en', 'trans', 'user_updated', 'User Deleted Successfully', '2019-02-22 19:12:25', '2019-02-22 21:00:12'),
(163, 0, 'ar', 'trans', 'create_new_department', 'إضافة قسم جديد', '2019-02-22 19:26:34', '2019-02-22 21:00:12'),
(164, 0, 'en', 'trans', 'create_new_department', 'Create New Department', '2019-02-22 19:26:44', '2019-02-22 21:00:12'),
(165, 0, 'ar', 'trans', 'delete', 'حذف', '2019-02-22 19:48:16', '2019-02-22 21:00:12'),
(166, 0, 'en', 'trans', 'delete', 'Delete', '2019-02-22 19:48:21', '2019-02-22 21:00:12'),
(167, 0, 'ar', 'trans', 'add_departments_first', 'الرجاء إضافة أقسام أولاً', '2019-02-22 20:35:22', '2019-02-22 21:00:12'),
(168, 0, 'en', 'trans', 'add_departments_first', 'Please add departments first', '2019-02-22 20:35:30', '2019-02-22 21:00:12'),
(169, 0, 'ar', 'trans', 'department', 'القسم', '2019-02-22 20:50:57', '2019-02-22 21:00:12'),
(170, 0, 'en', 'trans', 'department', 'Department', '2019-02-22 20:51:03', '2019-02-22 21:00:12'),
(171, 0, 'ar', 'trans', 'create_new_user', 'إضافة مستخدم جديد', '2019-02-22 20:51:29', '2019-02-22 21:00:12'),
(172, 0, 'en', 'trans', 'create_new_user', 'Create New User', '2019-02-22 20:51:36', '2019-02-22 21:00:12'),
(173, 0, 'ar', 'trans', 'select_department_please', 'إختر قسم من فضلك', '2019-02-22 20:52:33', '2019-02-22 21:00:12'),
(174, 0, 'en', 'trans', 'select_department_please', 'Select a department please', '2019-02-22 20:52:39', '2019-02-22 21:00:12'),
(175, 0, 'ar', 'trans', 'update_user', 'تعديل مستخدم', '2019-02-22 20:59:58', '2019-02-22 21:00:12'),
(176, 0, 'en', 'trans', 'update_user', 'Update User', '2019-02-22 21:00:03', '2019-02-22 21:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_02_21_163319_create_admins_table', 1),
(2, '2019_02_21_163537_create_departments_table', 1),
(3, '2019_02_21_163540_create_users_table', 1),
(4, '2014_04_02_193005_create_translations_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `department_id` int(10) UNSIGNED NOT NULL,
  `ar_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_department_id_foreign` (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `department_id`, `ar_name`, `en_name`, `email`, `phone`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 7, 'إسم إسم', 'name name', 'user@user.com', '213216465', '$2y$10$wgyh10tp2Hsi5CsFX0x3GOwfy4z95.ahtAnHgVSLeHDxesBLpU2zK', NULL, '2019-02-22 20:57:19', '2019-02-22 20:57:19'),
(2, 9, 'إسم إسم', 'name name', 'user@user.com', '213216465', '$2y$10$mxWzXv3KMgRksApGLpQNH.wNRLdR7vq3bBKpw9K21JsqDM5dQPYjq', NULL, '2019-02-22 20:58:30', '2019-02-22 21:09:54');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
