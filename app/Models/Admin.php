<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $fillable =
        [
            'ar_name','en_name','email','phone','password'
        ];
}
