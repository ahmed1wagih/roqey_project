<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable =
        [
            'department_id','ar_name','en_name','email','phone','password'
        ];


    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id')->select('id',lang().'_name as name');
    }
}
