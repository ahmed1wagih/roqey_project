<?php

use Illuminate\Support\Facades\Config;


function unique_file($fileName)
{
    $fileName = str_replace(' ','-',$fileName);
    return time() . uniqid().'-'.$fileName;
}


function lang()
{
    return App::getLocale();
}


function admin()
{
    return Auth::guard('admin')->user();
}



function user()
{
    return Auth::guard('user')->user();
}


