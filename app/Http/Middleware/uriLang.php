<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class uriLang
{

    public function handle($request, Closure $next)
    {
        $uri_lang = $request->segment(1);
        $config = Config::get('languages');


        if($uri_lang &&  in_array($uri_lang,array_keys($config)))
        {
            App::setLocale($uri_lang);
            return $next($request);
        }
        else
        {
            return back();
        }
    }

}
