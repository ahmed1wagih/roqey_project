<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{


    public function handle($request, Closure $next)
    {
            if (Auth::guard('admin')->user())
            {
                return $next($request);
            }
            else
            {
                return redirect('/'.lang().'/admin/login/index');
            }
    }


}
