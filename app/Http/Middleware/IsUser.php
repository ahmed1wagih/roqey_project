<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsUser
{


    public function handle($request, Closure $next)
    {
            if (Auth::guard('user')->user())
            {
                return $next($request);
            }
            else
            {
                return redirect('/'.lang().'/user/login/index');
            }
    }


}
