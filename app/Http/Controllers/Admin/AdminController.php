<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index()
    {
        $admins = Admin::get();
        return view('admins.admins.index', compact('admins'));
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'ar_name' => 'required',
                'en_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'password' => 'required'
            ],
            [
                'ar_name.required' => 'عفواً,الإسم بالعربية مطلوب',
                'en_name.required' => 'عفواً,الإسم بالإنجليزية مطلوب',
                'email.required' => 'عفواً,الإسم بالعربية مطلوب',
                'email.email' => 'عفواً,البريد الإلكتروني غير صحيح',
                'phone.required' => 'عفواً,رقم الهاتف مطلوب مطلوب',
                'password.required' => 'عفواً,كلمة المرور مطلوبة',
                'password.confirmed' => 'عفواً,كلمة المرور غير متطابقة',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        $admin = Admin::create
        (
            [
                'ar_name' => $request->ar_name,
                'en_name' => $request->en_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
            ]
        );

        return response()->json(['status' => 'success', 'msg' => trans('trans.admin_added'), 'data' => $admin]);
    }


    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'admin_id' => 'required|exists:admins,id',
                'ar_name' => 'sometimes',
                'en_name' => 'sometimes',
                'email' => 'sometimes|email',
                'phone' => 'sometimes',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        $admin = Admin::where('id',$request->admin_id)->first();
            if($request->ar_name) $admin->ar_name = $request->ar_name;
            if($request->en_name) $admin->en_name = $request->en_name;
            if($request->email) $admin->email = $request->email;
            if($request->phone) $admin->phone = $request->phone;
        $admin->save();

        return response()->json(['status' => 'success', 'msg' => trans('trans.admin_updated'), 'data' => $admin]);
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'admin_id' => 'required|exists:admins,id|not_in:1',
            ],
            [
                'admin_id.not_in' => 'لا يمكن حذف الأدمن الرئيسي عشان اللوجين'
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        Admin::where('id',$request->admin_id)->delete();

        return response()->json(['status' => 'success', 'msg' => trans('trans.admin_deleted')]);
    }
}
