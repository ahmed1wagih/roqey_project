<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = User::get();
        $departments = Department::select('id',lang().'_name as name')->get();

        return view('admins.users.index', compact('users','departments'));
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'department_id' => 'required|exists:departments,id',
                'ar_name' => 'required',
                'en_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'password' => 'required|confirmed'
            ],
            [
                'department_id.required' => 'عفواً,القسم مطلوب',
                'ar_name.required' => 'عفواً,الإسم بالعربية مطلوب',
                'en_name.required' => 'عفواً,الإسم بالإنجليزية مطلوب',
                'email.required' => 'عفواً,الإسم بالعربية مطلوب',
                'email.email' => 'عفواً,البريد الإلكتروني غير صحيح',
                'phone.required' => 'عفواً,رقم الهاتف مطلوب مطلوب',
                'password.required' => 'عفواً,كلمة المرور مطلوبة',
                'password.confirmed' => 'عفواً,كلمة المرور غير متطابقة',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        $user = User::create
        (
            [
                'department_id' => $request->department_id,
                'ar_name' => $request->ar_name,
                'en_name' => $request->en_name,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
            ]
        );

        $user['department_name'] = $user->department->name;

        return response()->json(['status' => 'success', 'msg' => trans('trans.user_added'), 'data' => $user]);
    }


    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
                'department_id' => 'required|exists:departments,id',
                'ar_name' => 'sometimes',
                'en_name' => 'sometimes',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        $user = User::where('id',$request->user_id)->first();
            if($request->department_id) $user->department_id = $request->department_id;
            if($request->ar_name) $user->ar_name = $request->ar_name;
            if($request->en_name) $user->en_name = $request->en_name;
            if($request->email) $user->email = $request->email;
            if($request->phone) $user->phone = $request->phone;
        $user->save();

        $user['department_name'] = $user->department->name;

        return response()->json(['status' => 'success', 'msg' => trans('trans.user_updated'), 'data' => $user]);
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required|exists:users,id',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        User::where('id',$request->user_id)->delete();

        return response()->json(['status' => 'success', 'msg' => trans('trans.user_deleted')]);
    }
}
