<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    public function index()
    {
        $departments = Department::get();
        return view('admins.departments.index', compact('departments'));
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'ar_name' => 'required',
                'en_name' => 'required',
            ],
            [
                'ar_name.required' => 'عفواً,الإسم بالعربية مطلوب',
                'en_name.required' => 'عفواً,الإسم بالإنجليزية مطلوب',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        $department = Department::create
        (
            [
                'ar_name' => $request->ar_name,
                'en_name' => $request->en_name,
            ]
        );

        return response()->json(['status' => 'success', 'msg' => trans('trans.department_added'), 'data' => $department]);
    }


    public function update(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'department_id' => 'required|exists:departments,id',
                'ar_name' => 'sometimes',
                'en_name' => 'sometimes',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        $department = Department::where('id',$request->department_id)->first();
            if($request->ar_name) $department->ar_name = $request->ar_name;
            if($request->en_name) $department->en_name = $request->en_name;
        $department->save();

        return response()->json(['status' => 'success', 'msg' => trans('trans.department_updated'), 'data' => $department]);
    }


    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'department_id' => 'required|exists:departments,id',
            ]
        );

        if($validator->fails())
        {
            $errors = [];
            foreach($validator->errors()->toArray() as $key => $value)
            {
                $errors[] = $value[0];
            }

            return response()->json(['status' => 'failed', 'msg' => $errors]);
        }

        Department::where('id',$request->department_id)->delete();

        return response()->json(['status' => 'success', 'msg' => trans('trans.department_deleted')]);
    }
}
