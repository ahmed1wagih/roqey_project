<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('admins.auth.index');
    }


    public function check(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );



        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect(lang().'/admin/dashboard');
        }
        else
        {
            if(App::getLocale() == 'ar') $msg = 'خطأ في البيانات,حاول مرة أخري';
            else $msg = 'Invalid Credentials,please try again';

            return back()->with('error',$msg);
        }
    }


    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect(lang().'/admin/login/index');
    }
}
