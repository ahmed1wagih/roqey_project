<?php

namespace App\Http\Controllers\User;

use App\Models\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DepartmentController extends Controller
{
    public function index()
    {
        $department = Department::where('id', Auth::guard('user')->user()->department_id)->first();
        return view('users.departments.index', compact('department'));
    }
}
