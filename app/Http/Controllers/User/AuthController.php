<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('users.auth.index');
    }


    public function check(Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required'
            ]
        );



        if(Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect(lang().'/user/dashboard');
        }
        else
        {
            if(App::getLocale() == 'ar') $msg = 'خطأ في البيانات,حاول مرة أخري';
            else $msg = 'Invalid Credentials,please try again';

            return back()->with('error',$msg);
        }
    }

    public function logout()
    {
        Auth::guard('user')->logout();
        return redirect(lang().'/user/login/index');
    }
}
